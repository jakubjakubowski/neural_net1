/*
 * ConsolePrinter.cpp
 *
 *  Created on: 17.08.2017
 *      Author: jjk
 */


#include "ConsolePrinter.hpp"

using namespace std;

void ConsolePrinter::printDivisionLine ()
{
    vector<unique_ptr<IAnswerer>>::iterator it_answerer = answerers.begin();
    for(;it_answerer != answerers.end(); it_answerer++)
    {
        cout << string(46,'=') << endl;
    }
}

void ConsolePrinter::printAnswerFields ()
{
    for (double y = 1.0; y > -1.1; y -= 0.1)
    {

        vector<unique_ptr<IAnswerer>>::iterator it_answerer = answerers.begin();
        for(;it_answerer != answerers.end(); it_answerer++)
        {
            for (double x = -1.0; x < 1.0; x += 0.1)
            {
                printSingleAnswer(it_answerer->get(), x, y);
            }
            cout<<"    ";
        }
        cout<<endl;
    }
}

void ConsolePrinter::addAnswerer(IAnswerer *ans)
{
//    answerers.push_back(move(ans));//std::unique_ptr<IAnswerer>(*ans));
}

void ConsolePrinter::printResults ()
{
    printDivisionLine();
    printPaddedNames();
    printAnswerFields();
}

void ConsolePrinter::printSingleAnswer (IAnswerer *ans, double x, double y)
{
    double d[]= {x,y};

    if (ans->answer(d) > 0) {
//    cout << "\033[31m1\033[39m ";
    }
    else {
//    cout << "\033[32m0\033[39m ";
    }
}

void ConsolePrinter::printPaddedNames ()
{
    vector<unique_ptr<IAnswerer>>::iterator it_answerer = answerers.begin();
    for(;it_answerer != answerers.end(); it_answerer++)
    {
//      cout << left << cout.width(46) << it_answerer->get()->getName() << endl;
        cout << left << cout.width(46) << (*it_answerer)->getName() << endl;
    }
    cout<<endl;
}
