/*
 * ReferenceFrame.hpp
 *
 *  Created on: 17.08.2017
 *      Author: jjk
 */

#ifndef REFERENCEFRAME_HPP_
#define REFERENCEFRAME_HPP_

using namespace std;
#include <random>
#include "IAnswerer.hpp"


class ReferenceFrame: public IAnswerer
{
private:
//    vector<IAnswerer> answerers;
    string name;
    default_random_engine generator;
    uniform_real_distribution<double> distribution;

public:
    ReferenceFrame(string newName);
    virtual ~ReferenceFrame();

    void addBarier(IAnswerer &ans);
    double genCoord();
    virtual string getName();
    virtual double answer(double x[]);





};



#endif /* REFERENCEFRAME_HPP_ */
