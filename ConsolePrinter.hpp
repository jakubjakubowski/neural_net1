/*
 * ConsolePrinter.hpp
 *
 *  Created on: 17.08.2017
 *      Author: jjk
 */

#ifndef CONSOLEPRINTER_HPP_
#define CONSOLEPRINTER_HPP_

#include "IAnswerer.hpp"
#include <vector>
#include <memory>

using namespace std;

class ConsolePrinter
{
private:
    vector<unique_ptr<IAnswerer>> answerers;

//    IAnswerer *a;
    void printDivisionLine();
    void printAnswerFields();

public:
    void addAnswerer(IAnswerer *ans);
    void printResults();
    void printSingleAnswer(IAnswerer* ans, double x, double y);
    void printPaddedNames();


};

#endif /* CONSOLEPRINTER_HPP_ */
