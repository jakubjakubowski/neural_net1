/*
 * IAnswerer.hpp
 *
 *  Created on: 17.08.2017
 *      Author: jjk
 */

#ifndef IANSWERER_HPP_
#define IANSWERER_HPP_

#include <iostream>

using namespace std;


class IAnswerer
{
public:
    IAnswerer();
    virtual ~IAnswerer();
    virtual double answer(double x[]) = 0;
    virtual string getName();
};

#endif /* IANSWERER_HPP_ */
