/*
 * Perceptron.h
 *
 *  Created on: 17.08.2017
 *      Author: jjk
 */

#ifndef PERCEPTRON_HPP_
#define PERCEPTRON_HPP_

class Perceptron
{
private:
    double weights[];
public:
    Perceptron ();
    virtual ~Perceptron ();
};

#endif /* PERCEPTRON_HPP_ */
